<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index() {
		$this->load->view('welcome_message');
	}

	public function cari_kode_klasifikasi() {
		$g = $this->input->get();
		$q = !empty($g['search']) ? $g['search'] : "";

	    $res = [];
		if ($q != "") {
	        $this->db->like('klasifikasi', $q);
	        $this->db->select('*');
	        $get_klasifikasi = $this->db->get('klasifikasi')->result_array();

	        if (!empty($get_klasifikasi)) {
	            foreach ($get_klasifikasi as $k) {
	                $s['id']   = $k['id'];
	                $s['text'] = $k['klasifikasi'];
	                $res[]     = $s;
	            }
	        }
		}

		header('Content-Type: application/json');
        echo json_encode(["results" => $res]);
	}
}
