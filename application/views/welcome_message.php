<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	
    <script src="https://suratku.kulonprogokab.go.id/aset/vendors/jquery/js/jquery.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>

	<style type="text/css">
	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Welcome to Select 2!</h1>

	<div id="body">
		<select name="select2" id="select2" style="width: 200px"></select>
	</div>
</div>

<script type="text/javascript">
	$('#select2').select2({
	    multiple: false,
	    closeOnSelect: true,
	    allowClear: false,
	    ajax: {
	        url: '<?=base_url();?>index.php/welcome/cari_kode_klasifikasi',
	        data: function (params) {
	            var query = {
	                search: params.term,
	            }
	            return query;
	        }
	    }
	});
</script>


</body>
</html>